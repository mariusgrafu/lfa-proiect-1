#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

class Link{
    char l; // letter l links to the node with the id n_id
    int n_id;

    public:
        Link(){
            l = 0;
            n_id = 0;
        }
        Link(char _l, int _n_id){
            l = _l;
            n_id = _n_id;
        }
        bool hasChar(char c){
            return c == l;
        }
        int getId(){
            return n_id;
        }
};

class Node{
    bool is_last;
    vector<Link> links;

    public:
        Node(){
            is_last = 0;
        }

        void setIsLast(bool isLast = 1){
            is_last = isLast;
        }
        void pushLink(Link newLink){
            links.push_back(newLink);
        }
        bool isLast(){
            return is_last;
        }
        int gotoLink(char l){
            for(int i = 0; i < links.size(); ++i){
                if(links[i].hasChar(l)) return links[i].getId();
            }
            return -1;
        }
};

class AFD{
    int q; // how many states
    string e; // the alphabet
    int q0; // first state
    vector<Node> nodes;

    public:
        AFD(){
            q = 0;
            e = "";
            q0 = 0;
        }

        void read(char * file){
            ifstream in(file);
            in >> q;
            in >> e;
            in >> q0;
            nodes = vector<Node>(q);
            int finalCnt, fState;
            in >> finalCnt;
            for(int i = 0; i < finalCnt; ++i){
                in >> fState;
                nodes[fState].setIsLast();
            }
            int linksCnt, x, y;
            char l;
            in >> linksCnt;
            for(int i = 0; i < linksCnt; ++i){
                in >> x >> y >> l;
                Link NL = Link(l, y);
                nodes[x].pushLink(NL);
            }
        }

        void parseCmd(string cmd){
            static int c_id = q0; // current id
            static string tested = "";
            if(cmd == "L"){
                cmd.erase(0, 1);
            }
            if(!cmd.length()){
                if(nodes[c_id].isLast())
                    cout << "Correct!\n";
                else
                    cout << "Incorrect!\n";
                tested = "";
                c_id = q0;
                return;
            }
            int next_id = nodes[c_id].gotoLink(cmd[0]);
            if(next_id == -1){
                tested += cmd[0];
                tested += "<-- unknown command! - ";
                cout << tested << "Incorrect!\n";
                tested = "";
                c_id = q0;
                return;
            }
            tested += cmd[0];
            c_id = next_id;
            cmd.erase(0, 1);
            parseCmd(cmd);
        }

};

void getCmds(char *file, AFD A){
    ifstream in(file);
    int cmdsCnt;
    string cmd;
    in >> cmdsCnt;
    for(int i = 0; i < cmdsCnt; ++i){
        in >> cmd;
        cout << cmd << " - ";
        A.parseCmd(cmd);
    }
}

int main()
{
    AFD A;
    A.read("date.in");
    getCmds("cmds.in", A);
    cout << "\n\n";
    AFD B;
    B.read("date2.in");
    getCmds("cmds2.in", B);
    return 0;
}

